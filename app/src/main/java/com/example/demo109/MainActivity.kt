package com.example.demo109

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        floatingActionButton.setOnClickListener {
            startActivity<SecondActivity>()
            open_toast()
        }
        floatingActionButton2.setOnClickListener {
            open_alert()
        }
        floatingActionButton3.setOnClickListener {
            startActivity<ThirdActivity>()
        }

        println("MainActivity onCreate")

    }

    fun open_alert() {
        alert("Hiya", "歪比歪比?") {
            yesButton { toast("yes") }
            noButton { toast("no") }
        }.show()
    }

    fun open_toast(){
        toast("toast")
        longToast("long toast")
    }



    override fun onStart() {
        super.onStart()
        println("MainActivity onStart")
    }

    override fun onResume() {
        super.onResume()
        println("MainActivity onResume")
    }

    override fun onPause() {
        super.onPause()
        println("MainActivity onPause")
    }

    override fun onStop() {
        super.onStop()
        println("MainActivity onStop")
    }

    override fun onRestart() {
        super.onRestart()
        println("MainActivity onRestart")
    }

    override fun onDestroy() {
        super.onDestroy()
        println("MainActivity onDestroy")
    }
}

