package com.example.demo109

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

/**
 * Created by Wachsbeere on 2020/3/28.
 */

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        println("SecondActivity onCreate")

    }

    override fun onStart() {
        super.onStart()
        println("SecondActivity onStart")
    }

    override fun onResume() {
        super.onResume()
        println("SecondActivity onResume")
    }

    override fun onPause() {
        super.onPause()
        println("SecondActivity onPause")
    }

    override fun onStop() {
        super.onStop()
        println("SecondActivity onStop")
    }

    override fun onRestart() {
        super.onRestart()
        println("SecondActivity onRestart")
    }

    override fun onDestroy() {
        super.onDestroy()
        println("SecondActivity onDestroy")
    }
}