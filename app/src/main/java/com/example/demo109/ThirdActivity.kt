package com.example.demo109

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

/**
 * Created by Wachsbeere on 2020/3/28.
 */

class ThirdActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_third)

        println("ThirdActivity onCreate")

    }

    override fun onStart() {
        super.onStart()
        println("ThirdActivity onStart")
    }

    override fun onResume() {
        super.onResume()
        println("ThirdActivity onResume")
    }

    override fun onPause() {
        super.onPause()
        println("ThirdActivity onPause")
    }

    override fun onStop() {
        super.onStop()
        println("ThirdActivity onStop")
    }

    override fun onRestart() {
        super.onRestart()
        println("ThirdActivity onRestart")
    }

    override fun onDestroy() {
        super.onDestroy()
        println("ThirdActivity onDestroy")
    }
}